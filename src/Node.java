import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Node {

	private int numeroEstacao;

	private Linha linhaAtual;

	private Map<Node,Node> baldeacao = new HashMap<>();

	private double g_scores;

	private Node parent;

	private List<Edge> adjacencies = new ArrayList<>();;

	private double h_scores;

	private double f_scores;

	private List<Linha> linhas = new ArrayList<>();
	
	private boolean bald = false;

	public Node(int numeroEstacao) {
		this.numeroEstacao = numeroEstacao;
	}

	public int getNumeroEstacao() {
		return numeroEstacao;
	}

	public void setNumeroEstacao(int numeroEstacao) {
		this.numeroEstacao = numeroEstacao;
	}

	public Linha getLinhaAtual() {
		return linhaAtual;
	}

	public void setLinhaAtual(Linha linhaAtual) {
		this.linhaAtual = linhaAtual;
	}

	public double getG_scores() {
		return g_scores;
	}

	public void setG_scores(double g_scores) {
		this.g_scores = g_scores;
	}

	public Node getParent() {
		return parent;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}

	public List<Edge> getAdjacencies() {
		return adjacencies;
	}

	public void setAdjacencies(List<Edge> adjacencies) {
		this.adjacencies = adjacencies;
	}

	public double getH_scores() {
		return h_scores;
	}

	public void setH_scores(double h_scores) {
		this.h_scores = h_scores;
	}

	public double getF_scores() {
		return f_scores;
	}

	public void setF_scores(double f_scores) {
		this.f_scores = f_scores;
	}

	public Map<Node,Node> getBaldeacao() {
		return baldeacao;
	}

	public void setBaldeacao(Map<Node,Node> baldeacao) {
		this.baldeacao = baldeacao;
	}

	public List<Linha> getLinhas() {
		return linhas;
	}

	public void setLinhas(List<Linha> linhas) {
		this.linhas = linhas;
	}

	public boolean isBald() {
		return bald;
	}

	public void setBald(boolean bald) {
		this.bald = bald;
	}
	
	
	

}
